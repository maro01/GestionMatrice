#include "..\Headers\CExceptionFileNotFound.h"

CExceptionFileNotFound::CExceptionFileNotFound() : CException()
{
	pcEFNFMessage = (char*)malloc(sizeof(char) * strlen("CExceptionFileNotFound raised")+1);
	strcpy_s(pcEFNFMessage, sizeof("CExceptionFileNotFound raised"), pcEFNFMessage);
}

CExceptionFileNotFound::CExceptionFileNotFound(CExceptionFileNotFound& pEFNFArg) : CException(pEFNFArg)
{
	pcEFNFMessage = pEFNFArg.pcEFNFMessage;
}

CExceptionFileNotFound::~CExceptionFileNotFound()
{
	free(pcEFNFMessage);
}



void CExceptionFileNotFound::EAfficherErreur()
{
	if (EGetCodeErreur() == 0) printf("%s\n", pcEFNFMessage);
	else printf("CExceptionFileNotFound not raised");
}

void CExceptionFileNotFound::EFNFSetMessage(char* pcMessage)
{
	strcpy_s(pcEFNFMessage, sizeof(pcMessage), pcMessage);
}
