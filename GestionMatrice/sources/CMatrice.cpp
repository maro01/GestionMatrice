#include "../Headers/CMatrice.h"

template<typename MType>
 CMatrice<MType>::CMatrice():ptMMat(NULL), nMNblignes(0), nMNbcolonnes(0){}

template<typename MType>
 CMatrice<MType>::CMatrice(CMatrice& pMArg):ptMMat(pMArg.ptMMat), nMNblignes(pMArg.nMNblignes), nMNbcolonnes(pMArg.nMNbcolonnes) {}

 template<typename MType>
 CMatrice<MType>::~CMatrice()
{
	 MType** ptPtr = MGetmat();
	 for (int i = 0; i < MGetNblignes() ; i++) free(ptPtr[i]);
	 free(ptPtr);
}

template<typename MType>
unsigned int CMatrice<MType>::MGetNblignes()
{
	return nMNblignes;
}

template<typename MType>
unsigned int CMatrice<MType>::MGetNbcolonnes()
{
	return nMNbcolonnes;
}

template <class MType>
MType** CMatrice<MType>::MGetmat()
{
	return ptMMat;
}

template<typename MType>
void CMatrice<MType>::MSetNblignes(unsigned int nArg)
{
	nMNblignes = nArg;
}

template<typename MType>
void CMatrice<MType>::MSetNbcolonnes(unsigned int nArg)
{
	nMNbcolonnes = nArg;
}

template <class MType>
void CMatrice<MType>::MSetmat(MType** ptArg)
{
	ptMMat = ptArg;
}

template<class MType>
void CMatrice<MType>::MAfficherMatrice()
{
	MType** pMAArg = MGetmat();
	std::cout << "Affichage Matrice :" << std::endl;
	for (int i = 0; i < MGetNblignes(); i++)
	{
		for (int j = 0; j < MGetNbcolonnes(); j++)
		{
			std::cout << pMAArg[i][j] << " ";
		}
		std::cout << std::endl;
	}
}