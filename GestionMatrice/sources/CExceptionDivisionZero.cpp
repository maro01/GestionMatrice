#include "..\Headers\CExceptionDivisionZero.h"


CExceptionDivisionZero::CExceptionDivisionZero() : CException()
{
	pcEDZMessage = (char*)malloc(sizeof(char) * strlen("CExceptionDivisionZero raised") + 1);
	strcpy_s(pcEDZMessage,sizeof("CExceptionDivisionZero raised"),"CExceptionDivisionZero raised");
}

CExceptionDivisionZero::CExceptionDivisionZero(CExceptionDivisionZero & pEDZArg) : CException(pEDZArg)
{
	pcEDZMessage = pEDZArg.pcEDZMessage;
}

CExceptionDivisionZero::~CExceptionDivisionZero()
{
	free(pcEDZMessage);
}

void CExceptionDivisionZero::EAfficherErreur()
{
	if (EGetCodeErreur() == 0) printf("%s\n", pcEDZMessage);
	else printf("CExceptionDivisionZero not raised");
}

void CExceptionDivisionZero::EDZSetMessage(char* pcMessage)
{
   strcpy_s(pcEDZMessage, sizeof(pcMessage), pcMessage);
}
