#include "..\Headers\CExceptionTypeMatrice.h"


 CExceptionTypeMatrice::CExceptionTypeMatrice() : CException()
{
	 pcETMMessage = (char*)malloc(sizeof(char) * strlen("CExceptionTypeMatrice raised") + 1);
	 strcpy_s(pcETMMessage, sizeof("CExceptionTypeMatrice raised"), "CExceptionTypeMatrice raised");
}

 CExceptionTypeMatrice::CExceptionTypeMatrice(CExceptionTypeMatrice& pETMArg) : CException(pETMArg)
{
	 pcETMMessage = pETMArg.pcETMMessage;
}

 CExceptionTypeMatrice::~CExceptionTypeMatrice()
 {
	 free(pcETMMessage);
 }



void CExceptionTypeMatrice::EAfficherErreur()
{
	if (EGetCodeErreur() == 0) printf("%s\n", pcETMMessage);
	else printf("CExceptionTypeMatrice not raised");
}

void CExceptionTypeMatrice::ETMSetMessage(char* pcMessage)
{
	strcpy_s(pcETMMessage, sizeof(pcMessage), pcMessage);
}
