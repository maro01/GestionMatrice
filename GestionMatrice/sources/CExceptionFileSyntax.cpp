#include "..\Headers\CExceptionFileSyntax.h"


 CExceptionFileSyntax::CExceptionFileSyntax() : CException()
{
	 pcEFSMessage = (char*)malloc(sizeof(char) * strlen("CExceptionFileSyntax raised")+1);
	 strcpy_s(pcEFSMessage, sizeof("CExceptionFileSyntax raised"), "CExceptionFileSyntax raised");
}

 CExceptionFileSyntax::CExceptionFileSyntax(CExceptionFileSyntax& pEFSArg) : CException(pEFSArg)
{
	 pcEFSMessage = pEFSArg.pcEFSMessage;
}

 CExceptionFileSyntax::~CExceptionFileSyntax()
 {
	 free(pcEFSMessage);
 }



void CExceptionFileSyntax::EAfficherErreur()
{
	if (EGetCodeErreur() == 0) printf("%s\n", pcEFSMessage);
	else printf("CExceptionFileSyntax not raised");
}

void CExceptionFileSyntax::EFSSetMessage(char* pcMessage)
{
	strcpy_s(pcEFSMessage, sizeof(pcMessage), pcMessage);
}
