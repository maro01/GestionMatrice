#include "..\Headers\CException.h"

CException::CException() : eECodeErreur(0){}

CException::CException(CException& pEArg) : eECodeErreur(pEArg.eECodeErreur){}

int CException::EGetCodeErreur()
{
	return eECodeErreur;
}

void CException::EModifierCodeErreur(int eArg)
{
	eECodeErreur = eArg;
}

