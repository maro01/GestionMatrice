#include <iostream>
#include "../GestionMatrice/Headers/CMatrice.h"
#include "../GestionMatrice/sources/CMatrice.cpp"


int main()
{
	std::cout << "Gestion Matrices!\n" << std::endl;
	int** mat;
	mat = (int**)malloc(sizeof(int*) * 3);
	for (size_t i = 0; i < 3; i++)
	{
		mat[i] = (int*)malloc(sizeof(int) * 3);
	}

	mat[0][0] = 5; mat[0][1] = 2; mat[0][2] = 3;
	mat[1][0] = 4; mat[1][1] = 5; mat[1][2] = 6;
	mat[2][0] = 7; mat[2][1] = 8; mat[2][2] = 9;

	CMatrice<int> * matrice;
	matrice = new CMatrice<int>();
	matrice->MSetNbcolonnes(3);
	matrice->MSetNblignes(3);
	matrice->MSetmat(mat);
	matrice->MAfficherMatrice();

}