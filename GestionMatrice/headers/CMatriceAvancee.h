#ifndef C_MATRICE_AVANCEE_H
#define C_MATRICE_AVANCEE_H

#include "CMatrice.h"

template<class MType>
class CMatriceAvancee : public CMatrice<MType>
{
public:

	 CMatriceAvancee<MType>();

	 CMatriceAvancee<MType>(CMatriceAvancee<MType> & pMAArg);

	 CMatriceAvancee<MType>(char* pcFilePath);

	CMatriceAvancee<MType>* MATranspose();

	template<typename MArg>
	CMatriceAvancee<MType>* operator/(MArg MAArg);

	CMatriceAvancee<MType>* operator/(CMatriceAvancee<MType> & pMAArg);

	template<typename MArg>
	CMatriceAvancee<MType>* operator*(MArg MAArg);

	CMatriceAvancee<MType>* operator*(CMatrice<MType> & tC);
	
	template<typename MArg>
	CMatriceAvancee<MType>* operator+(MArg MAArg);

	CMatriceAvancee<MType>* operator+(CMatriceAvancee & pMAArg);

	template<typename MArg>
	CMatriceAvancee<MType>* operator-(MArg MAArg);

	CMatriceAvancee<MType>* operator-(CMatriceAvancee & pMAArg);

};
#endif
