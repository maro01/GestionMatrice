#ifndef C_MATRICE_FICHIER_H
#define C_MATRICE_FICHIER_H

#include "CMatriceAvancee.h"

class CMatriceFichier
{
private:

	char* pcMFFilePath;

public:
	 CMatriceFichier();

	 CMatriceFichier(CMatriceFichier& pMFArg);

	 CMatriceFichier(char* pcArg);

	 ~CMatriceFichier();

	void MFSetFilePath(char* pcArg);

	char* MFGetFilePath();

	template<class MType>
	CMatriceAvancee<MType>* MFGetMatriceAvancee();

};
#endif
