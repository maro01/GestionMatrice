#ifndef C_MATRICE_H
#define C_MATRICE_H

#include <iostream>
#include <fstream>
#include <vector>

template <class MType>
class CMatrice
{

private:

	MType** ptMMat;

	unsigned int nMNblignes;

	unsigned int nMNbcolonnes;

public:
	 CMatrice<MType>();
	 
	 CMatrice<MType>(CMatrice& pMArg);

	 CMatrice<MType>(char* pcFilePath);

	 ~CMatrice<MType>();

	unsigned int MGetNblignes();

	unsigned int MGetNbcolonnes();

	MType** MGetmat();

	void MSetNblignes(unsigned int nArg);

	void MSetNbcolonnes(unsigned int nArg);

	void MSetmat(MType** ptArg);

	void MAfficherMatrice();

};
#endif
